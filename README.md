## Ports (local -> host)

### Traefik:
 	81 -> 80
	8080 -> 8080

### Loki:
 	3100 -> 3100

### Grafana:
 	3000 -> 3000

#### Gitea:
	3200 -> 3000
	222 -> 22

### Odoo:
	8069 -> 8069
 
### Wordpress:
 	8001 -> 80
